package ma.octo.assignement.domain;

import java.math.BigDecimal;
import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(name = "COMPTE")
public class Compte {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(length = 16, unique = true)
  private String nrCompte;

  private String rib;

  @Column(precision = 16, scale = 2)
  private BigDecimal solde;

  @ManyToOne
  @JoinColumn(name = "utilisateur_id")
  private Utilisateur utilisateur;

  public Compte() {}

  @Override
  public boolean equals(Object o) {
    if (o == this) return true;
    if (!(o instanceof Compte)) {
      return false;
    }
    Compte compte = (Compte) o;
    return (
      Objects.equals(id, compte.id) &&
      Objects.equals(nrCompte, compte.nrCompte) &&
      Objects.equals(rib, compte.rib) &&
      Objects.equals(solde, compte.solde) &&
      Objects.equals(utilisateur, compte.utilisateur)
    );
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, nrCompte, rib, solde, utilisateur);
  }

  @Override
  public String toString() {
    return (
      "{" +
      " id='" +
      getId() +
      "'" +
      ", nrCompte='" +
      getNrCompte() +
      "'" +
      ", rib='" +
      getRib() +
      "'" +
      ", solde='" +
      getSolde() +
      "'" +
      ", utilisateur='" +
      getUtilisateur() +
      "'" +
      "}"
    );
  }

  public String getNrCompte() {
    return nrCompte;
  }

  public void setNrCompte(String nrCompte) {
    this.nrCompte = nrCompte;
  }

  public String getRib() {
    return rib;
  }

  public void setRib(String rib) {
    this.rib = rib;
  }

  public BigDecimal getSolde() {
    return solde;
  }

  public void setSolde(BigDecimal solde) {
    this.solde = solde;
  }

  public Utilisateur getUtilisateur() {
    return utilisateur;
  }

  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
