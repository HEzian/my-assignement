package ma.octo.assignement.domain;

import javax.persistence.*;

@Entity
@Table(name = "VERSEMENT")
// @DiscriminatorValue("VERSEMENT")
public class Versement extends Event {

  @Column
  private String nomPrenomEmetteur;

  public Versement() {}

  public String getNomPrenomEmetteur() {
    return nomPrenomEmetteur;
  }

  public void setNomPrenomEmetteur(String NomPrenomEmetteur) {
    this.nomPrenomEmetteur = NomPrenomEmetteur;
  }

  @Override
  public String toString() {
    return (
      super.toString() +
      " NomPrenomEmetteur='" +
      getNomPrenomEmetteur() +
      "'" +
      "}"
    );
  }
}
