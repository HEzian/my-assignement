package ma.octo.assignement.domain;

import javax.persistence.*;

@Entity
@Table(name = "VIREMENT")
// @DiscriminatorValue("VIREMENT")
public class Virement extends Event {

  @ManyToOne
  private Compte compteEmetteur;

  public Virement() {}

  public void setCompteEmetteur(Compte compteEmetteur) {
    this.compteEmetteur = compteEmetteur;
  }

  public Compte getCompteEmetteur() {
    return this.compteEmetteur;
  }

  @Override
  public String toString() {
    return (
      super.toString() + " compteEmetteur='" + getCompteEmetteur() + "'" + "}"
    );
  }
}
