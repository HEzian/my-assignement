package ma.octo.assignement.domain;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import javax.persistence.*;

@Entity
// @Table(name = "EVENT")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
// @DiscriminatorColumn(name = "event_type")
public abstract class Event {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  protected Long id;

  @Column(precision = 16, scale = 2, nullable = false)
  protected BigDecimal montantEvent;

  @Column
  @Temporal(TemporalType.TIMESTAMP)
  protected Date dateExecution;

  @ManyToOne
  protected Compte compteBeneficiaire;

  @Column(length = 200)
  protected String motifEvent;

  public Event() {}

  public Event(
    BigDecimal montantEvent,
    Date dateExecution,
    Compte compteBeneficiaire,
    String motifEvent
  ) {
    this.montantEvent = montantEvent;
    this.dateExecution = dateExecution;
    this.compteBeneficiaire = compteBeneficiaire;
    this.motifEvent = motifEvent;
  }

  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public BigDecimal getMontantEvent() {
    return this.montantEvent;
  }

  public void setMontantEvent(BigDecimal montantEvent) {
    this.montantEvent = montantEvent;
  }

  public Date getDateExecution() {
    return this.dateExecution;
  }

  public void setDateExecution(Date dateExecution) {
    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    try {
      this.dateExecution = formatter.parse(formatter.format(dateExecution));
    } catch (ParseException e) {
      e.printStackTrace();
    }
  }

  public Compte getCompteBeneficiaire() {
    return this.compteBeneficiaire;
  }

  public void setCompteBeneficiaire(Compte compteBeneficiaire) {
    this.compteBeneficiaire = compteBeneficiaire;
  }

  public String getMotifEvent() {
    return this.motifEvent;
  }

  public void setMotifEvent(String motifEvent) {
    this.motifEvent = motifEvent;
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) return true;
    if (!(o instanceof Event)) {
      return false;
    }
    Event event = (Event) o;
    return (
      Objects.equals(id, event.id) &&
      Objects.equals(montantEvent, event.montantEvent) &&
      Objects.equals(dateExecution, event.dateExecution) &&
      Objects.equals(compteBeneficiaire, event.compteBeneficiaire) &&
      Objects.equals(motifEvent, event.motifEvent)
    );
  }

  @Override
  public int hashCode() {
    return Objects.hash(
      id,
      montantEvent,
      dateExecution,
      compteBeneficiaire,
      motifEvent
    );
  }

  @Override
  public String toString() {
    return (
      "{" +
      " id='" +
      getId() +
      "'" +
      ", montantEvent='" +
      getMontantEvent() +
      "'" +
      ", dateExecution='" +
      getDateExecution() +
      "'" +
      ", compteBeneficiaire='" +
      getCompteBeneficiaire() +
      "'" +
      ", motifEvent='" +
      getMotifEvent() +
      "' "
    );
  }
}
