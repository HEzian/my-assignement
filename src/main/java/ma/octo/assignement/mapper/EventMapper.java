package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;

//regroupement des 2 methodes
public class EventMapper {

  private VirementDto virementDto;
  private VersementDto versementDto;

  public VirementDto map(Virement virement) {
    virementDto = new VirementDto();
    virementDto.setNrCompteEmetteur(virement.getCompteEmetteur().getNrCompte());
    virementDto.setDate(virement.getDateExecution());
    virementDto.setMotif(virement.getMotifEvent());
    virementDto.setMontant(virement.getMontantEvent());
    virementDto.setNrCompteBeneficiaire(
      virement.getCompteBeneficiaire().getNrCompte()
    );
    return virementDto;
  }

  public VersementDto map(Versement versement) {
    versementDto = new VersementDto();
    versementDto.setNomPrenomEmetteur(versementDto.getNomPrenomEmetteur());
    versementDto.setDate(versement.getDateExecution());
    versementDto.setMotif(versement.getMotifEvent());
    virementDto.setMontant(versement.getMontantEvent());
    versementDto.setRib(versement.getCompteBeneficiaire().getRib());
    return versementDto;
  }
}
