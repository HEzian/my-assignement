package ma.octo.assignement.repository;

import java.util.Date;
import java.util.List;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VirementRepository extends JpaRepository<Virement, Long> {
  Long countByDateExecutionAndCompteEmetteur(Date date, Compte compteEmetteur);
  List<Virement> findByCompteEmetteur(Compte compteEmetteur);
}
