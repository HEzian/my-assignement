package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Event;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EventRepository extends JpaRepository<Event, Long> {
    // List<Event> findByEventType(String eventType);
}
