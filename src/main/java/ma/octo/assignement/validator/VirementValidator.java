package ma.octo.assignement.validator;

import java.math.BigDecimal;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.VirementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VirementValidator extends EventValidator {

  private final long MAXIMUM_VIREMENT_PAR_JOUR = 10;

  @Autowired
  VirementRepository virementRepository;

  public Virement validateVirement(Object target)
    throws CompteNonExistantException, TransactionException {
    super.validate(target);

    VirementDto virementDto = (VirementDto) target;

    compteBeneficiaire =
      compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire());

    if (compteBeneficiaire == null) {
      System.out.println("Compte Bénéficiaire Non existant");
      throw new CompteNonExistantException("Compte Bénéficiaire Non existant");
    }

    Compte compteEmetteur = compteRepository.findByNrCompte(
      virementDto.getNrCompteEmetteur()
    );

    if (compteEmetteur == null) {
      System.out.println("Compte Emetteur Non existant");
      throw new CompteNonExistantException("Compte Emetteur Non existant");
    }

    if (
      (compteEmetteur.getSolde().subtract(virementDto.getMontant())).compareTo(
          BigDecimal.valueOf(0)
        ) <
      0
    ) {
      throw new TransactionException("Solde insuffisant pour l'utilisateur");
    }

    compteEmetteur.setSolde(
      compteEmetteur.getSolde().subtract(virementDto.getMontant())
    );
    compteRepository.save(compteEmetteur);

    // verification du nombre de virement
    if (
      virementRepository.countByDateExecutionAndCompteEmetteur(
        virementDto.getDate(),
        compteEmetteur
      ) ==
      MAXIMUM_VIREMENT_PAR_JOUR
    ) {
      throw new TransactionException("Maximum de transaction par jour atteint");
    }
    Virement virement = new Virement();
    virement.setDateExecution(virementDto.getDate());
    virement.setCompteBeneficiaire(compteBeneficiaire);
    virement.setCompteEmetteur(compteEmetteur);
    virement.setMontantEvent(virementDto.getMontant());
    virement.setMotifEvent(virementDto.getMotif()); //Ajout du motif

    compteBeneficiaire.setSolde(
      compteBeneficiaire.getSolde().add(virementDto.getMontant())
    );
    compteRepository.save(compteBeneficiaire);
    virement = virementRepository.save(virement);

    auditService.auditVirement(
      "Virement depuis " +
      virementDto.getNrCompteEmetteur() +
      " vers " +
      virementDto.getNrCompteBeneficiaire() +
      " d'un montant de " +
      virementDto.getMontant().toString()
    );
    return virement;
  }
}
