package ma.octo.assignement.validator;

import java.math.BigDecimal;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.EventDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.AuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EventValidator {

  private static final int MONTANT_MAXIMAL = 10000;
  private static final int MONTANT_MINIMAL = 10;

  @Autowired
  protected CompteRepository compteRepository;

  protected Compte compteBeneficiaire = null;

  @Autowired
  protected AuditService auditService;

  public void validate(Object target)
    throws CompteNonExistantException, TransactionException {
    EventDto dto = (EventDto) target;
    if (dto.getMontant() == null) {
      System.out.println("Montant vide");
      throw new TransactionException("Montant vide");
    } else if (dto.getMontant().equals(BigDecimal.valueOf(0))) { //modification de intvalue
      System.out.println("Montant vide");
      throw new TransactionException("Montant vide");
    } else if (dto.getMontant().intValue() < MONTANT_MINIMAL) { //transformation en constante
      System.out.println("Montant minimal de virement non atteint");
      throw new TransactionException("Montant minimal de virement non atteint");
    } else if (dto.getMontant().intValue() > MONTANT_MAXIMAL) {
      System.out.println("Montant maximal de virement dépassé");

      throw new TransactionException("Montant maximal de virement dépassé");
    }

    if (dto.getMotif() == null) {
      System.out.println("Motif vide");
      throw new TransactionException("Motif vide");
    } else if (dto.getMotif().trim().equals("")) {
      throw new TransactionException("Motif vide");
    } else if (dto.getMotif().length() < 0) {
      System.out.println("Motif vide");
      throw new TransactionException("Motif vide");
    }
  }
}
