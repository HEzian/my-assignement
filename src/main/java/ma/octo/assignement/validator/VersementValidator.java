package ma.octo.assignement.validator;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.VersementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VersementValidator extends EventValidator {

  @Autowired
  VersementRepository versementRepository;

  public Versement validateVersement(Object target)
    throws CompteNonExistantException, TransactionException {
    super.validate(target);
    VersementDto versementDto = (VersementDto) target;
    Compte compteBeneficiaire = compteRepository.findByRib(
      versementDto.getRib()
    );

    if (versementDto.getNomPrenomEmetteur() == null) {
      System.out.println(" Nom et prénom Emetteur vide");
      throw new CompteNonExistantException("Nom et prénom Emetteur vide");
    }

    if (versementDto.getNomPrenomEmetteur().length() < 0) {
      System.out.println("Nom et prénom émetteur vide");
      throw new TransactionException("Nom et prénom émetteur vide");
    }

    if (compteBeneficiaire == null) {
      System.out.println("Compte Bénéficiaire Non existant");
      throw new CompteNonExistantException("Compte Bénéficiaire Non existant");
    }

    compteBeneficiaire.setSolde(
      compteBeneficiaire.getSolde().add(versementDto.getMontant())
    );
    compteRepository.save(compteBeneficiaire);

    Versement versement = new Versement();

    versement.setNomPrenomEmetteur(versementDto.getNomPrenomEmetteur());
    versement.setDateExecution(versementDto.getDate());
    versement.setCompteBeneficiaire(compteBeneficiaire);
    versement.setMontantEvent(versementDto.getMontant());
    versement.setMotifEvent(versementDto.getMotif());

    versement = versementRepository.save(versement);

    auditService.auditVersement(
      "Versement de " +
      versementDto.getNomPrenomEmetteur() +
      " vers " +
      versementDto.getNrCompteBeneficiaire() +
      " d'un montant de " +
      versementDto.getMontant().toString()
    );
    return versement;
  }
}
