package ma.octo.assignement.web;

import java.util.List;
import ma.octo.assignement.domain.AuditEvent;
import ma.octo.assignement.service.AuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController(value = "/audits")
@RequestMapping(value = "/audits")
public class AuditController {

  @Autowired
  private AuditService auditService;

  @GetMapping("listerAudits")
  List<AuditEvent> loadAllAudit() {
    return auditService.loadAll();
  }
}
