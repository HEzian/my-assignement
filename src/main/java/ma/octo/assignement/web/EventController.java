package ma.octo.assignement.web;

import java.util.List;
import ma.octo.assignement.domain.Event;
import ma.octo.assignement.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController(value = "/events")
@RequestMapping(value = "/events")
public class EventController {

  @Autowired
  private EventService eventService;

  //Systeme de nommage identique
  @GetMapping("listerEvents")
  List<Event> loadAll() {
    return eventService.loadAll();
  }
}
