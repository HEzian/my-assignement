package ma.octo.assignement.web;

import java.util.List;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.VirementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController(value = "/virements")
@RequestMapping(path = "/virements")
public class VirementController {

  @Autowired
  private VirementService virementService;

  @GetMapping("listerVirements")
  List<Virement> loadAllVirement() {
    return virementService.loadAll();
  }

  @PostMapping("/executerVirements")
  @ResponseStatus(HttpStatus.CREATED)
  public void createVirementTransaction(@RequestBody VirementDto virementDto)
    throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
    virementService.createTransaction(virementDto);
  }
}
