package ma.octo.assignement.web;

import java.util.List;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.service.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController(value = "/utilisateurs")
@RequestMapping(value = "/utilisateurs")
public class UtilisateurController {

  @Autowired
  private UtilisateurService utilisateurService;

  @GetMapping("listerUtilisateurs")
  List<Utilisateur> loadAllUtilisateur() {
    return utilisateurService.loadAllUtilisateur();
  }
}
