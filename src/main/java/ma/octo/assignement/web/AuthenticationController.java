package ma.octo.assignement.web;

import ma.octo.assignement.dto.LoginRequestDto;
import ma.octo.assignement.service.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController(value = "/authentication")
@RequestMapping(value = "/auth")
public class AuthenticationController {

  @Autowired
  private UtilisateurService utilisateurService;

  @PostMapping("/login")
  public ResponseEntity<Authentication> login(
    @RequestBody LoginRequestDto requestDto
  )
    throws Exception {
    return new ResponseEntity<>(
      utilisateurService.login(requestDto),
      HttpStatus.OK
    );
  }
}
