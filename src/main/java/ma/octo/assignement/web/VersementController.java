package ma.octo.assignement.web;

import java.util.List;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.VersementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController(value = "/versements")
@RequestMapping(value = "/versements")
public class VersementController {

  @Autowired
  private VersementService versementService;

  @GetMapping("listerVersements")
  List<Versement> loadAllVersement() {
    return versementService.loadAll();
  }

  @PostMapping("/executerVersements")
  @ResponseStatus(HttpStatus.CREATED)
  public void createVersementTransaction(
    @RequestBody VersementDto versementDto
  )
    throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
    versementService.createTransaction(versementDto);
  }
}
