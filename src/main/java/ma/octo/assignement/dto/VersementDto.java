package ma.octo.assignement.dto;

public class VersementDto extends EventDto {

    private String NomPrenomEmetteur;
    private String rib;

    public String getNomPrenomEmetteur() {
        return NomPrenomEmetteur;
    }

    public String getRib() {
        return rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public void setNomPrenomEmetteur(String NomPrenomEmetteur) {
        this.NomPrenomEmetteur = NomPrenomEmetteur;
    }
}
