package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ma.octo.assignement.domain.Event;

public class EventDto {
    protected String nrCompteBeneficiaire;
    protected String motif;
    protected BigDecimal montant;
    protected Date date;

    
    public String getNrCompteBeneficiaire() {
        return nrCompteBeneficiaire;
    }
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
          this.date = formatter.parse(formatter.format(date));
        } catch (ParseException e) {
          e.printStackTrace();
        }
    }
    public BigDecimal getMontant() {
        return montant;
    }
    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }
    public String getMotif() {
        return motif;
    }
    public void setMotif(String motif) {
        this.motif = motif;
    }
    public void setNrCompteBeneficiaire(String nrCompteBeneficiaire) {
        this.nrCompteBeneficiaire = nrCompteBeneficiaire;
    }


    public EventDto map(Event event){
        return null;
    }
}
