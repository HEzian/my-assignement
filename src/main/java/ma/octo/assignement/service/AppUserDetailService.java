package ma.octo.assignement.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;

@Service
public class AppUserDetailService implements UserDetailsService {
    
    @Autowired
    UtilisateurRepository userRepository;


    @Override
    @Transactional
    public UserDetails loadUserByUsername(String userName) {
        Utilisateur user = userRepository.findByUsername(userName).orElseThrow(()->
           new UsernameNotFoundException("User not found"));

        return user;
    }

}
