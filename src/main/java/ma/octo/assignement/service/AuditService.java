package ma.octo.assignement.service;

import ma.octo.assignement.domain.AuditEvent;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditEventRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.transaction.Transactional;

@Service
@Transactional
public class AuditService {

    Logger LOGGER = LoggerFactory.getLogger(AuditService.class);

    @Autowired
    private AuditEventRepository auditEventRepository;

    public void auditVirement(String message) {

        LOGGER.info("Audit de l'événement {}", EventType.VIREMENT);

        AuditEvent audit = new AuditEvent();
        audit.setEventType(EventType.VIREMENT);
        audit.setMessage(message);
        auditEventRepository.save(audit);
    }


    public void auditVersement(String message) {

        LOGGER.info("Audit de l'événement {}", EventType.VERSEMENT);

        AuditEvent audit = new AuditEvent();
        audit.setEventType(EventType.VERSEMENT);
        audit.setMessage(message);
        auditEventRepository.save(audit);
    }

    public List<AuditEvent> loadAll(){
        return auditEventRepository.findAll();
    }
}
