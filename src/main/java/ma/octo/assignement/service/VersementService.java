package ma.octo.assignement.service;

import java.util.List;
import javax.transaction.Transactional;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.validator.VersementValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

//use case versement
@Service
@Transactional
public class VersementService {

  Logger LOGGER = LoggerFactory.getLogger(VersementService.class);

  @Autowired
  private VersementRepository versementRepository;

  @Autowired
  VersementValidator validator;

  public List<Versement> loadAll() {
    List<Versement> all = versementRepository.findAll();

    if (CollectionUtils.isEmpty(all)) {
      return null;
    } else {
      return all;
    }
  }

  public Versement createTransaction(VersementDto versementDto)
    throws CompteNonExistantException, TransactionException {
    return validator.validateVersement(versementDto);
  }
}
