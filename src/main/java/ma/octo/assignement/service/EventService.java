package ma.octo.assignement.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Event;
import ma.octo.assignement.repository.EventRepository;

@Service
@Transactional
public class EventService {

    @Autowired
    private EventRepository eventRepository;

    public List<Event> loadAll() {
        List<Event> all = eventRepository.findAll();
        if (CollectionUtils.isEmpty(all)) {
          return null;
        } else {
          return all;
        }
      }
}
