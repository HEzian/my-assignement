package ma.octo.assignement.service;

import java.util.List;
import javax.transaction.Transactional;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.validator.VirementValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
@Transactional
public class VirementService {

  public static final int MONTANT_MAXIMAL = 10000;
  public static final int MONTANT_MINIMAL = 10;

  Logger LOGGER = LoggerFactory.getLogger(VirementService.class);

  @Autowired
  private VirementRepository virementRepository;


  @Autowired
  private VirementValidator validator;

  public List<Virement> loadAll() {
    List<Virement> all = virementRepository.findAll();

    if (CollectionUtils.isEmpty(all)) {
      return null;
    } else {
      return all;
    }
  }

  public Virement createTransaction(VirementDto virementDto)
    throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
    return validator.validateVirement(virementDto);
  }
}
