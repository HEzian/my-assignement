package ma.octo.assignement.service;

import java.util.List;
import javax.transaction.Transactional;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.LoginRequestDto;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
@Transactional
public class UtilisateurService {

  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  private UtilisateurRepository utilisateurRepository;

  public List<Utilisateur> loadAllUtilisateur() {
    List<Utilisateur> all = utilisateurRepository.findAll();

    if (CollectionUtils.isEmpty(all)) {
      return null;
    } else {
      return all;
    }
  }

  public Authentication login(LoginRequestDto loginRequest) throws Exception {
    Authentication authentication = authenticationManager.authenticate(
      new UsernamePasswordAuthenticationToken(
        loginRequest.getUsername(),
        loginRequest.getPassword()
      )
    );
    SecurityContextHolder.getContext().setAuthentication(authentication);
    return authentication;
  }
}
