package ma.octo.assignement.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.repository.CompteRepository;

@Service
@Transactional
public class CompteService {

    @Autowired
    private CompteRepository compteRepository;

    
    public List<Compte> loadAllCompte() {
      List<Compte> all = compteRepository.findAll();
  
      if (CollectionUtils.isEmpty(all)) {
        return null;
      } else {
        return all;
      }
    }

}
