package ma.octo.assignement.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.EventMapper;
import ma.octo.assignement.repository.VirementRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class VirementServiceTest {

  @Mock
  private VirementRepository virementRepository;

  @Mock
  private VirementService virementService;

  private Virement virement;
  private Compte compteEmetteur, compteBeneficiaire;
  private List<Virement> virements;

  @BeforeAll
  public void setUp() {
    virements = new ArrayList<>();

    compteEmetteur.setNrCompte("0987567768888767");
    compteEmetteur.setRib("RIB3");
    compteEmetteur.setSolde(BigDecimal.valueOf(1000));
    compteEmetteur.setUtilisateur(
      new Utilisateur("mockEmet", "Mock", "Emetteur", "M")
    );

    compteBeneficiaire.setNrCompte("0987567768888709");
    compteBeneficiaire.setRib("RIB4");
    compteBeneficiaire.setSolde(BigDecimal.valueOf(1000));
    compteBeneficiaire.setUtilisateur(
      new Utilisateur("mockBenf", "Mock", "Bénéfiicaire2", "M")
    );

    virement = new Virement();

    virement.setCompteEmetteur(compteEmetteur);
    virement.setCompteBeneficiaire(compteBeneficiaire);
    virement.setDateExecution(new Date());
    virement.setMontantEvent(BigDecimal.valueOf(1000));
    virement.setMotifEvent("Mock virement1");

    Virement virement2 = new Virement();
    virement2.setCompteEmetteur(compteEmetteur);
    virement2.setCompteBeneficiaire(compteBeneficiaire);
    virement2.setDateExecution(new Date());
    virement2.setMontantEvent(BigDecimal.valueOf(500));
    virement2.setMotifEvent("Mock virement2");
    virements.add(virement);
    virements.add(virement2);
  }

  @Test
  public void loadAllVirements() throws Exception {
    Mockito.when(virementRepository.findAll()).thenReturn(virements);
    List<Virement> mockRepositoryVirements = virementRepository.findAll();
    Mockito.when(virementService.loadAll()).thenReturn(mockRepositoryVirements);
    List<Virement> mockVirements = virementService.loadAll();
    assertNotNull(mockVirements);
    assertEquals(2, mockVirements.size());
    verify(virementRepository).findAll();
    verify(virementService).loadAll();
  }

  @Test
  public void executeVirement() throws Exception {
    VirementDto virementDto = new VirementDto();
    virementDto.setDate(new Date());
    virementDto.setMontant(BigDecimal.valueOf(500));
    virementDto.setMotif("Mock virement2");
    virementDto.setNrCompteEmetteur("0987567768888767");
    virementDto.setNrCompteBeneficiaire("0987567768888709");
    Mockito
      .when(virementService.createTransaction(virementDto))
      .thenReturn(virement);
    Virement mockVirement = virementService.createTransaction(virementDto);
    assertEquals(
      "0987567768888709",
      mockVirement.getCompteBeneficiaire().getNrCompte()
    );
    assertEquals(
      "0987567768888767",
      mockVirement.getCompteEmetteur().getNrCompte()
    );
    assertEquals("Mock virement2", mockVirement.getMotifEvent());
    assertEquals(500, mockVirement.getMontantEvent().intValue());
    verify(virementService).createTransaction(virementDto);
  }

  @Test
  public void executeVirementWithCompteNonExistantException() throws Exception {
    VirementDto virementDto = new VirementDto();
    virementDto.setDate(new Date());
    virementDto.setMontant(BigDecimal.valueOf(500));
    virementDto.setMotif("Mock virement2");
    virementDto.setNrCompteEmetteur("0987567768888767");
    virementDto.setNrCompteBeneficiaire("0987567768888700"); //wrong number
    try {
      Mockito
        .when(virementService.createTransaction(virementDto))
        .thenThrow(
          new CompteNonExistantException("Compte Bénéficiaire Inexistant")
        );
      assertNull(virementService.createTransaction(virementDto));
    } catch (CompteNonExistantException e) {
      assertEquals("Compte Bénéficiaire Inexistant", e.getMessage());
    }
    verify(virementService).createTransaction(virementDto);
  }

  @Test
  public void executeVirementWithSoldeDisponibleInsuffisantException()
    throws Exception {
    VirementDto virementDto = new VirementDto();
    virementDto.setDate(new Date());
    virementDto.setMontant(BigDecimal.valueOf(4000)); //montant > solde Emetteur
    virementDto.setMotif("Mock virement2");
    virementDto.setNrCompteEmetteur("0987567768888767");
    virementDto.setNrCompteBeneficiaire("0987567768888709");
    try {
      Mockito
        .when(virementService.createTransaction(virementDto))
        .thenThrow(
          new SoldeDisponibleInsuffisantException("Solde Insuffisant")
        );
      assertNull(virementService.createTransaction(virementDto));
    } catch (SoldeDisponibleInsuffisantException e) {
      assertEquals("Solde Insuffisant", e.getMessage());
    }
    verify(virementService).createTransaction(virementDto);
  }

  @Test
  public void executeVirementWithEmptyMotif() throws Exception {
    VirementDto virementDto = new VirementDto();
    virementDto.setDate(new Date());
    virementDto.setMontant(BigDecimal.valueOf(500));
    virementDto.setMotif(""); // Empty motif
    virementDto.setNrCompteEmetteur("0987567768888767");
    virementDto.setNrCompteBeneficiaire("0987567768888709");

    try {
      Mockito
        .when(virementService.createTransaction(virementDto))
        .thenThrow(new TransactionException("Motif vide"));
      assertNull(virementService.createTransaction(virementDto));
    } catch (TransactionException e) {
      assertEquals("Motif vide", e.getMessage());
    }
    verify(virementService).createTransaction(virementDto);
  }
}
