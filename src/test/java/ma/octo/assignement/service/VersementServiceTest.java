package ma.octo.assignement.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.VersementRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class VersementServiceTest {

  private Versement versement;
  private List<Versement> versements;
  private Compte compteBeneficiaire1, compteBeneficiaire2;

  @Mock
  private VersementService versementService;

  @Mock
  private VersementRepository versementRepository;

  @BeforeAll
  public void setUp() {
    versements = new ArrayList<>();
    versement = new Versement();
    Versement versement2 = new Versement();
    compteBeneficiaire1 = new Compte();
    compteBeneficiaire2 = new Compte();

    compteBeneficiaire1.setNrCompte("0987567768888767");
    compteBeneficiaire1.setRib("RIB3");
    compteBeneficiaire1.setSolde(BigDecimal.valueOf(1000));
    compteBeneficiaire1.setUtilisateur(
      new Utilisateur("mockBenf", "Mock", "Bénéfiicaire", "M")
    );

    compteBeneficiaire2.setNrCompte("0987567768888709");
    compteBeneficiaire2.setRib("RIB4");
    compteBeneficiaire2.setSolde(BigDecimal.valueOf(1000));
    compteBeneficiaire2.setUtilisateur(
      new Utilisateur("mockBenf", "Mock", "Bénéfiicaire2", "M")
    );

    versement.setCompteBeneficiaire(compteBeneficiaire1);
    versement.setDateExecution(new Date());
    versement.setMontantEvent(BigDecimal.valueOf(2000));
    versement.setMotifEvent("Mock virement");
    versement.setNomPrenomEmetteur("Mock Emetteur1");

    versement2.setCompteBeneficiaire(compteBeneficiaire2);
    versement2.setDateExecution(new Date());
    versement2.setMontantEvent(BigDecimal.valueOf(2000));
    versement2.setMotifEvent("Mock virement");
    versement2.setNomPrenomEmetteur("Mock Emetteur2");
    versements.add(versement);
    versements.add(versement2);
  }

  @Test
  public void loadAllVersements() throws Exception {
    Mockito.when(versementRepository.findAll()).thenReturn(versements);
    List<Versement> mockRepositoryList = versementRepository.findAll();
    Mockito.when(versementService.loadAll()).thenReturn(mockRepositoryList);
    List<Versement> mockVersements = versementService.loadAll();
    assertNotNull(mockVersements);
    assertEquals(2, mockVersements.size());
    verify(versementRepository).findAll();
    verify(versementService).loadAll();
  }

  @Test
  public void executeVersement() throws Exception {
    VersementDto versementDto = new VersementDto();
    versementDto.setDate(new Date());
    versementDto.setMontant(BigDecimal.valueOf(2000));
    versementDto.setMotif("Mock virement");
    versementDto.setNomPrenomEmetteur("Mock Emetteur1");
    versementDto.setRib("RIB3");

    Mockito
      .when(versementService.createTransaction(versementDto))
      .thenReturn(versement);
    Versement mockVersement = versementService.createTransaction(versementDto);
    assertEquals("RIB3", mockVersement.getCompteBeneficiaire().getRib());
    assertEquals("Mock Emetteur1", mockVersement.getNomPrenomEmetteur());
    assertEquals("Mock virement", mockVersement.getMotifEvent());
    assertEquals(2000, mockVersement.getMontantEvent().intValue());
    verify(versementService).createTransaction(versementDto);
  }

  @Test
  public void executeVersementWithCompteNonExistantException()
    throws Exception {
    VersementDto versementDto = new VersementDto();
    versementDto.setDate(new Date());
    versementDto.setMontant(BigDecimal.valueOf(2000));
    versementDto.setMotif("Mock virement");
    versementDto.setNomPrenomEmetteur("Mock Emetteur1");
    versementDto.setRib("RIB5");
    try {
      Mockito
        .when(versementService.createTransaction(versementDto))
        .thenThrow(
          new CompteNonExistantException("Compte Bénéficiaire Inexistant")
        );
      assertNull(versementService.createTransaction(versementDto));
    } catch (CompteNonExistantException e) {
      assertEquals("Compte Bénéficiaire Inexistant", e.getMessage());
    }
    verify(versementService).createTransaction(versementDto);
  }

  @Test
  public void executeVersementWithMontantMinimalInsuffisantException()
    throws Exception {
    VersementDto versementDto = new VersementDto();
    versementDto.setDate(new Date());
    versementDto.setMontant(BigDecimal.valueOf(0.5));
    versementDto.setMotif("Mock virement");
    versementDto.setNomPrenomEmetteur("Mock Emetteur1");
    versementDto.setRib("RIB3");

    try {
      Mockito
        .when(versementService.createTransaction(versementDto))
        .thenThrow(new TransactionException("Montant vide"));
      assertNull(versementService.createTransaction(versementDto));
    } catch (TransactionException e) {
      assertEquals("Montant vide", e.getMessage());
    }
    verify(versementService).createTransaction(versementDto);
  }

  @Test
  public void executeVersementWithEmptyMotif() throws Exception {
    VersementDto versementDto = new VersementDto();
    versementDto.setDate(new Date());
    versementDto.setMontant(BigDecimal.valueOf(2000));
    versementDto.setMotif("");
    versementDto.setNomPrenomEmetteur("Mock Emetteur1");
    versementDto.setRib("RIB3");

    try {
      Mockito
        .when(versementService.createTransaction(versementDto))
        .thenThrow(new TransactionException("Motif vide"));
      assertNull(versementService.createTransaction(versementDto));
    } catch (TransactionException e) {
      assertEquals("Motif vide", e.getMessage());
    }
    verify(versementService).createTransaction(versementDto);
  }
}
