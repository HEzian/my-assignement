package ma.octo.assignement.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.Assert;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class VirementRepositoryTest {

  @Mock
  private VirementRepository virementRepository;

  private Virement virement;
  private List<Virement> virements;

  private Compte compteEmetteur, compteBeneficiaire;

  @BeforeAll
  private void setUp() {
    virements = new ArrayList<>();

    compteEmetteur.setNrCompte("0987567768888767");
    compteEmetteur.setRib("RIB3");
    compteEmetteur.setSolde(BigDecimal.valueOf(1000));
    compteEmetteur.setUtilisateur(
      new Utilisateur("mockEmet", "Mock", "Emetteur", "M")
    );

    compteBeneficiaire.setNrCompte("0987567768888709");
    compteBeneficiaire.setRib("RIB4");
    compteBeneficiaire.setSolde(BigDecimal.valueOf(1000));
    compteBeneficiaire.setUtilisateur(
      new Utilisateur("mockBenf", "Mock", "Bénéfiicaire2", "M")
    );

    virement = new Virement();

    virement.setCompteEmetteur(compteEmetteur);
    virement.setCompteBeneficiaire(compteBeneficiaire);
    virement.setDateExecution(new Date());
    virement.setMontantEvent(BigDecimal.valueOf(1000));
    virement.setMotifEvent("Mock virement1");

    Virement virement2 = new Virement();
    virement2.setCompteEmetteur(compteEmetteur);
    virement2.setCompteBeneficiaire(compteBeneficiaire);
    virement2.setDateExecution(new Date());
    virement2.setMontantEvent(BigDecimal.valueOf(500));
    virement2.setMotifEvent("Mock virement2");
    virements.add(virement);
    virements.add(virement2);
  }

  @Test
  public void findOne() {
    Mockito
      .when(virementRepository.findById(1L))
      .thenReturn(Optional.of(virement));
    Optional<Virement> result = virementRepository.findById(1L);
    Assert.notNull(result, "Virement found");
    assertTrue(result.isPresent());
    verify(virementRepository).findById(1L);
  }

  @Test
  public void findAll() {
    Mockito.when(virementRepository.findAll()).thenReturn(virements);
    List<Virement> mockVirements = virementRepository.findAll();
    assertEquals(2, mockVirements.size());
    verify(virementRepository).findAll();
  }

  @Test
  public void save() {
    Mockito
      .when(virementRepository.saveAndFlush(virement))
      .thenReturn(virement);
    Virement v = virementRepository.saveAndFlush(virement);
    assertEquals(v.getMontantEvent(), virement.getMontantEvent());
    assertEquals(v.getCompteEmetteur(), virement.getCompteEmetteur());
    assertEquals(v.getCompteBeneficiaire(), virement.getCompteBeneficiaire());
    assertEquals(v.getMotifEvent(), virement.getMotifEvent());
    verify(virementRepository).saveAndFlush(virement);
  }
}
